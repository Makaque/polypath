import PolyPath.Functions;
import PolyPath.types.*;
import PolyPath.types.Vector;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.fail;

class FunctionsTest {


    private BiFunction<Vector, Vector, Boolean> vectorsEqual = (v1, v2) ->
            Functions.pointEquals.apply(v1.p1, v2.p1) && Functions.pointEquals.apply(v1.p2, v2.p2);
    private BiFunction<Collection<Vector>, Collection<Vector>, Boolean> containsAll = (set1, set2) ->
            set1.stream().anyMatch(v1 -> set2.stream().anyMatch(v2 -> vectorsEqual.apply(v1, v2)));
    private BiFunction<Collection<Vector>, Collection<Vector>, Boolean> setsMatch = (set1, set2) ->
            containsAll.apply(set1, set2) && containsAll.apply(set2, set1) && set1.size() == set2.size();


    @Test
    void listAppend() {
        List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        List<Integer> list2 = Functions.listAppend(list, 6);
        if (list2.size() != 6) {
            fail("Appended list does not contain the correct number of elements: " + list2);
        }
        if (list.size() != 5) {
            fail("Original list does not contain the correct number of elements: " + list);
        }

    }

    @Test
    void tail() {
        List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
        List<Integer> list2 = Functions.tail(list);
        if (list2.size() != 4) {
            fail("Appended list does not contain the correct number of elements: " + list2);
        }
        list2.remove(0);
        if (list2.size() != 3) {
            fail("Appended list does not contain the correct number of elements: " + list2);
        }
        if (list.size() != 5) {
            fail("Original list does not contain the correct number of elements: " + list);
        }
    }

    @Test
    void actions() {
        Point bl = new Point(1d, 1d);
        Point br = new Point(2d, 1d);
        Point tr = new Point(2d, 4d);
        Point tl = new Point(1d, 4d);

        List<Polygon> shapes = Collections.singletonList(new Polygon(Arrays.asList(
                bl, br, tr, tl
        )));

        Point start = new Point(0d, 0d);

        Point goal = new Point(3d, 3d);

        List<Point> visited = Collections.emptyList();

        Set<Vector> pathChoices = Functions.actions.apply(shapes).apply(goal).apply(visited).apply(start);

        List<Vector> goalChoices = Arrays.asList(
                new Vector(start, bl),
                new Vector(start, tl),
                new Vector(start, br)
        );
        System.out.println("pathChoices: " + pathChoices);
        if(!setsMatch.apply(pathChoices, goalChoices)){
            fail("Path choices and goal path choices don't match: " + pathChoices);
        }
    }

    @Test
    void linesTouch(){
        Line l1 = new Line(new Point(1d, 1d), new Point(3d, 3d));
        Line l2 = new Line(new Point(3d, 1d), new Point(1d, 3d));

        Line l3 = new Line(new Point(2d, 0d), new Point(0d, 2d));

        Boolean touches = Functions.linesTouch.apply(l1, l2);
        if(!touches){
            fail("Lines l1 and l2 should be touching: " + l1 + " " + l2);
        }

        touches = Functions.linesTouch.apply(l1, l3);
        if(!touches){
            fail("Lines l1 and l3 should be touching: " + l1 + " " + l3);
        }
    }

    @Test
    void lineIntersectsLine(){
        Line l1 = new Line(new Point(1d, 1d), new Point(3d, 3d));
        Line l2 = new Line(new Point(3d, 1d), new Point(1d, 3d));

        Line l3 = new Line(new Point(2d, 0d), new Point(0d, 2d));

        Boolean touches = Functions.lineIntersectsLine.apply(l1).apply(l2);
        if(!touches){
            fail("Lines l1 and l2 should be intersecting: " + l1 + " " + l2);
        }

        touches = Functions.lineIntersectsLine.apply(l1).apply(l3);
        if(!touches){
            fail("Lines l1 and l3 should be intersecting: " + l1 + " " + l3);
        }
    }

    @Test
    void boundingBoxesIntersect(){
        Line l1 = new Line(new Point(1d, 1d), new Point(3d, 3d));
        Line l2 = new Line(new Point(3d, 1d), new Point(1d, 3d));

        Line l3 = new Line(new Point(2d, 0d), new Point(0d, 2d));

        Boolean touches = Functions.boundingBoxesIntersect.apply(Functions.getBoundingBoxCorners.apply(l1)).apply(Functions.getBoundingBoxCorners.apply(l2));
        if(!touches){
            fail("Bounding boxes for l1 and l2 should be intersecting: " + l1 + " " + l2);
        }

        touches = Functions.boundingBoxesIntersect.apply(Functions.getBoundingBoxCorners.apply(l1)).apply(Functions.getBoundingBoxCorners.apply(l3));
        if(!touches){
            fail("Bounding boxes for l1 and l3 should be intersecting: " + l1 + " " + l3);
        }
    }

    @Test
    void getBoundingBoxCorners(){
        Line l1 = new Line(new Point(1d, 1d), new Point(3d, 3d));
        Pair<Point> l1Box = new Pair<>(new Point(1d, 1d), new Point(3d, 3d));
        Line l2 = new Line(new Point(3d, 1d), new Point(1d, 3d));
        Pair<Point> l2Box = new Pair<>(new Point(1d, 1d), new Point(3d, 3d));

        Line l3 = new Line(new Point(2d, 0d), new Point(0d, 2d));
        Pair<Point> l3Box = new Pair<>(new Point(0d, 0d), new Point(2d, 2d));

        Pair<Point> box1 = Functions.getBoundingBoxCorners.apply(l1);
        Pair<Point> box2 = Functions.getBoundingBoxCorners.apply(l2);
        Pair<Point> box3 = Functions.getBoundingBoxCorners.apply(l3);

        BiFunction<Pair<Point>, Pair<Point>, Boolean> boxesMatch = (pair1, pair2) ->
                Functions.pointEquals.apply(pair1.left, pair2.left)
                && Functions.pointEquals.apply(pair1.right, pair2.right);

        Boolean b1Match = boxesMatch.apply(l1Box, box1);
        Boolean b2Match = boxesMatch.apply(l2Box, box2);
        Boolean b3Match = boxesMatch.apply(l3Box, box3);

        if(!b1Match){
            fail("Boxes 1 don't match: " + box1);
        }

        if(!b2Match){
            fail("Boxes 2 don't match: " + box2);
        }

        if(!b3Match){
            fail("Boxes 3 don't match: " + box3);
        }
    }

    @Test
    void allVectorsFromStart(){
        Point bl = new Point(1d, 1d);
        Point br = new Point(2d, 1d);
        Point tr = new Point(2d, 4d);
        Point tl = new Point(1d, 4d);
        Point goal = new Point(3d, 3d);

        List<Point> points = (Arrays.asList(
                bl, br, tr, tl, goal
        ));

        Point start = new Point (0d, 0d);

        Set<Vector> expectedPaths = new HashSet<>(Arrays.asList(
                new Vector(start, bl),
                new Vector(start, br),
                new Vector(start, tr),
                new Vector(start, tl),
                new Vector(start, goal)
        ));

        Set<Vector> paths = Functions.allVectorsFromStart.apply(points).apply(start);

        if(!setsMatch.apply(expectedPaths, paths)){
            fail("Paths don't match expected: " + paths);
        }

    }

    @Test
    void intersectsShape(){
        Point bl = new Point(1d, 1d);
        Point br = new Point(2d, 1d);
        Point tr = new Point(2d, 4d);
        Point tl = new Point(1d, 4d);

        List<Polygon> shapes = Collections.singletonList(new Polygon(Arrays.asList(
                bl, br, tr, tl
        )));

        Point start = new Point(0d, 0d);

        Point goal = new Point(3d, 3d);

        List<MultiPair<Vector, Boolean>> expectedLineIntersections = Arrays.asList(
                new MultiPair<>(new Vector(start, bl), false),
                new MultiPair<>(new Vector(start, br), false),
                new MultiPair<>(new Vector(start, tr), true),
                new MultiPair<>(new Vector(start, tl), false),
                new MultiPair<>(new Vector(start, goal), true)
        );

        List<MultiPair<Vector, Boolean>> lineIntersections = expectedLineIntersections.stream()
                .map(lineInt -> new MultiPair<>(lineInt.left, Functions.lineIntersectsShape.apply(shapes).apply(lineInt.left)))
                .collect(Collectors.toList());

        for (int i = 0; i < lineIntersections.size(); i++) {
            if (lineIntersections.get(i).right != expectedLineIntersections.get(i).right){
                fail("Line intersection doesn't match expected: " + lineIntersections.get(i).left);
            }
        }
    }

    @Test
    void sortedSetTail() {
    }

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void uncurry() {
    }
}