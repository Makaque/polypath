import PolyPath.*;
import PolyPath.types.Pair;
import PolyPath.types.Point;
import PolyPath.types.Polygon;
import PolyPath.utils.IOMessage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

class TextStreamIOTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void readShapesSucceed() {
        String testInput = "1.4 2.3, 32 644, 67.6 23, 34 65; 12 32, 23 54, 32 12; 76 56,45 45;\n" +
                "324 545, 7676 454, 43 232;\n" +
                "324 545, 7676 454, 43 232;\n" +
                "d\n" +
                "0 0, 1000 1000";
        InputStream stream = new ByteArrayInputStream(testInput.getBytes(StandardCharsets.UTF_8));
        TextStreamIO io = new TextStreamIO(stream, System.out);
        io.readShapes().match((IOMessage.Success<List<Polygon>> s) -> null,
                (IOMessage.Error<List<Polygon>> e) -> {
                    e.t.printStackTrace();
                    fail("Encountered error");
                    return null;
                },
                (IOMessage.Quit<List<Polygon>> q) -> {
                    fail("wasn't supposed to quit here");
                    return null;
                });
    }

    @Test
    void readShapesQuit() {
        String testInput = "1.4 2.3, 32 644, 67.6 23, 34 65; 12 32, 23 54, 32 12; 76 56,45 45;\n" +
                "324 545, 7676 454, 43 232;\n" +
                "324 545, 7676 454, 43 232;\n" +
                "q\n" +
                "d\n" +
                "0 0, 1000 1000";
        InputStream stream = new ByteArrayInputStream(testInput.getBytes(StandardCharsets.UTF_8));
        TextStreamIO io = new TextStreamIO(stream, System.out);
        io.readShapes().match(
                (IOMessage.Success<List<Polygon>> s) -> {
                    fail("Wasn't supposed to succeed here");
                    return null;
                },
                (IOMessage.Error<List<Polygon>> e) -> {
                    e.t.printStackTrace();
                    fail("Encountered error");
                    return null;
                },
                (IOMessage.Quit<List<Polygon>> q) -> {
                    return null;
                });
    }

    @Test
    void readEndPoints() {
        String testInput =
                "0 0, \n" +
                        "1000\n" +
                        " 1000";
        InputStream stream = new ByteArrayInputStream(testInput.getBytes(StandardCharsets.UTF_8));
        TextStreamIO io = new TextStreamIO(stream, System.out);
        io.readEndPoints().match((IOMessage.Success<Pair<Point>> s) -> null,
                (IOMessage.Error<Pair<Point>> e) -> {
                    e.t.printStackTrace();
                    fail("Encountered error");
                    return null;
                },
                (IOMessage.Quit<Pair<Point>> q) -> {
                    fail("wasn't supposed to quit here");
                    return null;
                });
    }

    @Test
    void testNextShapesInput() {
        System.out.println("In test next shapes input");
        String testInput = "1 2, 6 4, 23 54;\n " +
                "48 32, 23 54, 34 53;\n" +
                "d";
        Scanner scan = new Scanner(testInput);
        TextStreamIO io = new TextStreamIO(System.in, System.out);
        try {
            Field nextShapesInputClass = io.getClass().getDeclaredField("nextShapesInput");
            nextShapesInputClass.setAccessible(true);
            Function<Scanner, Function<String, IOMessage<String>>> nextShapesInput = (Function<Scanner, Function<String, IOMessage<String>>>) nextShapesInputClass.get(io);
            IOMessage<String> result = nextShapesInput.apply(scan).apply("");
            String feedback = result.match(
                    (IOMessage.Success<String> s) -> {
                        System.out.println("is success");
                        return s.payload;
                    },
                    (IOMessage.Error<String> e) -> "Error: " + e.t.getMessage(),
                    (IOMessage.Quit<String> q) -> "Quit"
            );
            System.out.println("feedback: " + feedback);
            if (!result.isSuccess() || feedback.isEmpty()) {
                fail("Did not succeed or result is empty");
            }
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("No such field", e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Illegal access", e);
        }
    }

    @Test
    void testNextEndPointsInput() {
        System.out.println("In test next end point input");
        String testInput =
                "0 0, \n" +
                        "1000\n" +
                        " 1000";
        Scanner scan = new Scanner(testInput);
        TextStreamIO io = new TextStreamIO(System.in, System.out);
        try {
            Field nextEndPointsInputClass = io.getClass().getDeclaredField("nextEndPointsInput");
            nextEndPointsInputClass.setAccessible(true);
            Function<Scanner, Function<String, IOMessage<String>>> nextEndPointsInput = (Function<Scanner, Function<String, IOMessage<String>>>) nextEndPointsInputClass.get(io);
            IOMessage<String> result = nextEndPointsInput.apply(scan).apply("");
            String feedback = result.match(
                    (IOMessage.Success<String> s) -> {
                        System.out.println("is success");
                        return s.payload;
                    },
                    (IOMessage.Error<String> e) -> "Error: " + e.t.getMessage(),
                    (IOMessage.Quit<String> q) -> "Quit"
            );
            System.out.println("feedback: " + feedback);
            if (!result.isSuccess() || feedback.isEmpty()) {
                fail("Did not succeed or result is empty");
            }
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("No such field", e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Illegal access", e);
        }
    }


    @Test
    void testReadShapesInputSuccess() {

        System.out.println("In test next shapes input");
        String testInput = "1 2, 6 4, 23 54;\n " +
                "48 32, 23 54, 34 53;\n" +
                "d";
        Scanner scan = new Scanner(testInput);
        TextStreamIO io = new TextStreamIO(System.in, System.out);
        try {
            Field readShapesInputClass = io.getClass().getDeclaredField("readShapesInput");
            readShapesInputClass.setAccessible(true);
            Function<Scanner, IOMessage<String>> readShapesInput = (Function<Scanner, IOMessage<String>>) readShapesInputClass.get(io);
            IOMessage<String> result = readShapesInput.apply(scan);
            String feedback = result.match((IOMessage.Success<String> s) -> s.payload,
                    (IOMessage.Error<String> e) -> "Error: " + e.t.getMessage(),
                    (IOMessage.Quit<String> q) -> "Quit");
            System.out.print(feedback);
            System.out.println("");
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("No such field", e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Illegal access", e);
        }
    }

    @Test
    void testNextShapesInputQuit() {

        System.out.println("In test next shapes input");
        String testInput = "1 2, 6 4, 23 54;\n " +
                "48 32, 23 54, 34 53;\n" +
                "q\n" +
                "d";
        Scanner scan = new Scanner(testInput);
        TextStreamIO io = new TextStreamIO(System.in, System.out);
        try {
            Field readShapesInputClass = io.getClass().getDeclaredField("readShapesInput");
            readShapesInputClass.setAccessible(true);
            Function<Scanner, IOMessage<String>> readShapesInput = (Function<Scanner, IOMessage<String>>) readShapesInputClass.get(io);
            IOMessage<String> result = readShapesInput.apply(scan);
            String feedback = result.match((IOMessage.Success<String> s) -> s.payload,
                    (IOMessage.Error<String> e) -> "Error: " + e.t.getMessage(),
                    (IOMessage.Quit<String> q) -> "Quit");
            System.out.print(feedback);
            System.out.println("");
            if (!feedback.equals("Quit")) {
                fail("Failed to quit");
            }
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("No such field", e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Illegal access", e);
        }
    }

    @Test
    void testParseSuccessfulShapesInput() {

        System.out.println("In test parse successful shapes input");
        String testInput = "1 2, 6 4, 23 54;48 32, 23 54, 34 53;34 65, 23 54, 12 4;";
        TextStreamIO io = new TextStreamIO(System.in, System.out);
        try {
            Field parseSuccessfulShapesInputClass = io.getClass().getDeclaredField("parseSuccessfulShapesInput");
            parseSuccessfulShapesInputClass.setAccessible(true);
            Function<String, IOMessage<List<Polygon>>> parseSuccessfulShapesInput = (Function<String, IOMessage<List<Polygon>>>) parseSuccessfulShapesInputClass.get(io);
            IOMessage<List<Polygon>> result = parseSuccessfulShapesInput.apply(testInput);
            List<Polygon> feedback = result.match((IOMessage.Success<List<Polygon>> s) -> s.payload,
                    (IOMessage.Error<List<Polygon>> e) -> new ArrayList<Polygon>(),
                    (IOMessage.Quit<List<Polygon>> q) -> new ArrayList<Polygon>());
            System.out.print(feedback);
            System.out.println("");
            if (feedback.isEmpty()) {
                fail("Shapes were not successfully parsed");
            }
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("No such field", e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Illegal access", e);
        }
    }

    @Test
    void testEndPointsFinished() {

        System.out.println("In test end points finished");
        String testInput1 = "1 2, 6 4"; //true
        String testInput2 = "1.0 2, 6 4"; //true
        String testInput3 = "1 2.4, 6 4"; //true
        String testInput4 = "1 2    , 6 4"; //true
        String testInput5 = "1 2    , 6 4,"; //false
        String testInput6 = "1 2.    , 6 4"; //false
        TextStreamIO io = new TextStreamIO(System.in, System.out);
        try {
            Field endPointsFinishedClass = io.getClass().getDeclaredField("endPointsFinished");
            endPointsFinishedClass.setAccessible(true);
            Function<String, Boolean> endPointsFinished = (Function<String, Boolean>) endPointsFinishedClass.get(io);
            Boolean result1 = endPointsFinished.apply(testInput1);
            Boolean result2 = endPointsFinished.apply(testInput2);
            Boolean result3 = endPointsFinished.apply(testInput3);
            Boolean result4 = endPointsFinished.apply(testInput4);
            Boolean result5 = endPointsFinished.apply(testInput5);
            Boolean result6 = endPointsFinished.apply(testInput6);
            if (!result1 || !result2 || !result3 || !result4 || result5 || result6) {
                fail("" +
                        "result1: " + result1 +
                        "\nresult2: " + result2 +
                        "\nresult3: " + result3 +
                        "\nresult4: " + result4 +
                        "\nresult5: " + result5 +
                        "\nresult6: " + result6);
            }
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("No such field", e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Illegal access", e);
        }
    }

}