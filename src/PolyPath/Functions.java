package PolyPath;

import PolyPath.types.*;
import PolyPath.types.Vector;
import PolyPath.utils.Recursive;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created on 02/02/2018.
 */
public final class Functions {
    public static final Double epsilon = 0.000000001;

    public static <A, B, C> BiFunction<A, B, C> uncurry(Function<A, Function<B, C>> f) {

        return (A a, B b) -> f.apply(a).apply(b);

    }

    public static <A> List<A> listAppend(List<A> list, A item) {
        List<A> newList = new ArrayList<>();
        newList.addAll(list);
        newList.add(item);
        return newList;
    }

    public static <A> List<A> tail(List<A> list) {
        if (list.size() < 2) {
            return new ArrayList<>();
        } else {
            return new ArrayList<>(list.subList(1, list.size()));
        }
    }

    public static <A> SortedSet<A> sortedSetTail(SortedSet<A> set){
        SortedSet<A> tailSet = set.tailSet(set.first());
        return new TreeSet<>(tailSet);
    }

    public static <A> SortedSet<A> sortedSetAppendAll(SortedSet<A> set1, Collection<A> set2){
        SortedSet<A> mergedSet = new TreeSet<>(set1.comparator());
        mergedSet.addAll(set1);
        mergedSet.addAll(set2);
        return mergedSet;
    }

    public static final BiFunction<Double, Double, Boolean> dEquals = (d1, d2) -> (d1 >= d2 - epsilon) && (d1 <= d2 + epsilon);

    public static final Function<Double, Double> sqrt = Math::sqrt;

    public static final Function<Double, Double> square = x -> x * x;

    public static final Function<Line, Pair<Point>> getBoundingBoxCorners = line -> {
        Double smallestX = Math.min(line.p1.x, line.p2.x);
        Double largestX = Math.max(line.p1.x, line.p2.x);
        Double smallestY = Math.min(line.p1.y, line.p2.y);
        Double largestY = Math.max(line.p1.y, line.p2.y);
        Point bottomLeft = new Point(smallestX, smallestY);
        Point topRight = new Point(largestX, largestY);
        return new Pair<>(bottomLeft, topRight);
    };

    public static final Function<Point, Function<Point, Double>> pointProduct = p1 -> p2 ->
            p1.x * p2.y - p2.x * p1.y;

    public static final Function<Pair<Point>, Function<Pair<Point>, Boolean>> boundingBoxesIntersect = box1 -> box2 ->
            box1.left.x <= box2.right.x &&
                    box1.right.x >= box2.left.x &&
                    box1.left.y <= box2.right.y &&
                    box1.right.y >= box2.left.y;


    public static final Function<List<Point>, Function<Point, Set<Vector>>> allVectorsFromStart = points -> pointStart ->
            points.stream().map(p -> new Vector(pointStart, p)).collect(Collectors.toSet());

    public static final Function<Polygon, List<Line>> shapeToLineList = shape -> {
        if(shape.vertices.size() == 0){
            return new ArrayList<>();
        }
        List<Point> vertices = listAppend(shape.vertices, shape.vertices.get(0));
        Recursive<Function<List<Line>, Function<Point, Function<List<Point>, List<Line>>>>> nextLine = new Recursive<>();
        nextLine.func = acc -> p -> ps -> {
            if (ps.size() > 0) {
                return nextLine.func
                        .apply(listAppend(acc, new Line(p, ps.get(0))))
                        .apply(ps.get(0))
                        .apply(tail(ps));
            } else {
                return acc;
            }
        };

        return nextLine.func
                .apply(new ArrayList<>())
                .apply(vertices.get(0))
                .apply(tail(vertices));
    };

    public static final Function<Point, Function<Point, Point>> shiftPoint = point -> shiftVal ->
            new Point(point.x - shiftVal.x, point.y - shiftVal.y);

    public static final Function<Line, Vector> toOriginVector = line ->
            new Vector(Point.origin, shiftPoint.apply(line.p2).apply(line.p1));

    public static final Function<Function<Double, Boolean>, Function<Line, Function<Point, Boolean>>> isPointRelatedToLine =
            evaluator -> line -> point -> {
                Line shiftedLine = toOriginVector.apply(line);
                Point shiftedPoint = shiftPoint.apply(point).apply(line.p1);
                Double product = pointProduct.apply(shiftedLine.p2).apply(shiftedPoint);
                return evaluator.apply(product);
            };

    public static final Function<Line, Function<Point, Boolean>> isPointOnLine =
            isPointRelatedToLine.apply(product -> dEquals.apply(product, 0d));

    public static final Function<Line, Function<Point, Boolean>> isPointRightOfLine =
            isPointRelatedToLine.apply(product -> product < 0);


    public static final BiFunction<Line, Line, Boolean> lineTouchesLine = (line1, line2) -> {
        Function<Point, Boolean> onLine1 = isPointOnLine.apply(line1);
        Function<Point, Boolean> rightOfLine1 = isPointRightOfLine.apply(line1);
        return onLine1.apply(line2.p1)
                || onLine1.apply(line2.p2)
                || (rightOfLine1.apply(line2.p1) ^ rightOfLine1.apply(line2.p2));
    };

    public static final BiFunction<Line, Line, Boolean> linesTouch = (line1, line2) ->
            lineTouchesLine.apply(line1, line2) && lineTouchesLine.apply(line2, line1);


    public static final Function<Line, Function<Line, Boolean>> lineIntersectsLine = line1 -> line2 -> {
        Function<Pair<Point>, Boolean> checkIntersectsBox1 =
                getBoundingBoxCorners.andThen(boundingBoxesIntersect).apply(line1);
        Boolean boxesIntersect = getBoundingBoxCorners.andThen(checkIntersectsBox1).apply(line2);
        return boxesIntersect && linesTouch.apply(line1, line2);
    };

    public static final BiFunction<Point, Point, Boolean> pointEquals = (point1, point2) ->
            dEquals.apply(point1.x, point2.x) && dEquals.apply(point1.y, point2.y);


    public static final BiFunction<Line, Line, Boolean> lineEquals = (l1, l2) ->
            pointEquals.apply(l1.p1, l2.p1) && pointEquals.apply(l1.p2, l2.p2)
            || pointEquals.apply(l1.p1, l2.p2) && pointEquals.apply(l1.p2, l2.p1);

    public static final Function<Line, Function<Line, Boolean>> hasSameEndpoint = line1 -> line2 ->
            pointEquals.apply(line1.p1, line2.p1) ||
                    pointEquals.apply(line1.p1, line2.p2) ||
                    pointEquals.apply(line1.p2, line2.p1) ||
                    pointEquals.apply(line1.p2, line2.p2);

    public static final Function<List<Polygon>, Function<Line, List<Line>>> getIntersectLines = shapes -> line -> {
        Function<Line, Boolean> isEndpointLine = hasSameEndpoint.apply(line);
        Function<Line, Boolean> intersectsLine = lineIntersectsLine.apply(line);
        return
                shapes.stream()
                        .flatMap(p -> shapeToLineList.apply(p).stream())
                        .filter(intersectsLine::apply)
                        .filter(l -> !isEndpointLine.apply(l))
                        .collect(Collectors.toList());
    };


    public static final BiFunction<Point, Polygon, Boolean> pointInShape = (point, shape) ->
            shape.vertices.stream().anyMatch(p -> pointEquals.apply(p, point));

    public static final BiFunction<Line, Polygon, Boolean> bothLineEndPointsInShape = (line, shape) ->
            pointInShape.apply(line.p1, shape) && pointInShape.apply(line.p2, shape);

    public static final BiFunction<Line, Polygon, Boolean> lineAlongEdge = (line, shape) ->
            shapeToLineList.apply(shape).stream().anyMatch(l -> lineEquals.apply(l, line));

    public static final BiFunction<Line, Polygon, Boolean> isLineInsideShape = (line, shape) ->
            bothLineEndPointsInShape.apply(line, shape) && !lineAlongEdge.apply(line, shape);


    public static final Function<List<Polygon>, Function<Line, Boolean>> lineIntersectsShape =
            shapes -> line ->
                    getIntersectLines.apply(shapes).apply(line).size() > 0
            || shapes.stream().anyMatch(shape -> isLineInsideShape.apply(line, shape));


    public static final Function<List<Polygon>, Function<Point, Function<List<Point>, Function<Point, Set<Vector>>>>> actions =
            shapes -> pointGoal -> visited -> pointStart -> {
                Function<Line, Boolean> intersectsShape = lineIntersectsShape.apply(shapes);
                List<Point> allPoints = listAppend(shapes.stream().flatMap(p -> p.vertices.stream()).collect(Collectors.toList()), pointGoal)
                        .stream()
                        .filter(p -> visited.stream().noneMatch(pv -> pointEquals.apply(p,pv)))
                        .collect(Collectors.toList());
                return
                        allVectorsFromStart.apply(allPoints).apply(pointStart)
                                .stream()
                                .filter(v -> !intersectsShape.apply(v))
                                .collect(Collectors.toSet());
            };

    public static final Function<Point, Function<Point, Double>> squareDistanceBetweenPoints = (pointFrom) -> (pointTo) ->
            square.apply(pointTo.x - pointFrom.x) + square.apply(pointTo.y - pointFrom.y);

    public static final Function<Point, Function<Point, Double>> distanceBetweenPoints = point1 -> point2 ->
            sqrt.apply(uncurry(squareDistanceBetweenPoints).apply(point1, point2));

    public static final Function<Vector, Double> squareVectorMagnitude = (v) -> uncurry(squareDistanceBetweenPoints).apply(v.p1, v.p2);

    public static final Function<Vector, Double> vectorMagnitude = squareVectorMagnitude.andThen(sqrt);

    public static final Function<List<Vector>, Double> getPathLength = vectors ->
            vectors.stream().map(vectorMagnitude).reduce(0d, (a, b) -> a + b);

    public static final Function<AStarNode, List<AStarNode>> reconstructPath = node -> {
        List<AStarNode> nodes = Collections.singletonList(node);
        if(node.parent == null){
            return nodes;
        } else {
            return listAppend(Functions.reconstructPath.apply(node.parent), node);
        }
    };

}
