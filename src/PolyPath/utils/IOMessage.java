package PolyPath.utils;

import java.util.function.Consumer;
import java.util.function.Function;

public abstract class IOMessage<T> {

    private IOMessage() {
    }

    public <U> IOMessage<U> flatMap(Function<Success<T>, IOMessage<U>> mapper) {
        return
                this.match(mapper,
                        (Error<T> e) -> new Error<U>(e.t),
                        (Quit<T> q) -> new Quit<U>());
    }

    public <U> IOMessage<U> map(Function<Success<T>, U> mapper) {
        return
                this.match((Success<T> s) -> new Success<U>(mapper.apply(s)),
                        (Error<T> e) -> new Error<U>(e.t),
                        (Quit<T> q) -> new Quit<U>());
    }

    public void forEach(Consumer<Success<T>> consumer) {
        Object nothing = this.match((Success<T> s) -> {
                    consumer.accept(s);
                    return null;
                },
                (Error<T> e) -> null,
                (Quit<T> q) -> null);
    }

    public Boolean isSuccess() {
        return false;
    }

    public Boolean isError() {
        return false;
    }

    public Boolean isQuit() {
        return false;
    }

    public abstract <M> M match(Function<Success<T>, M> s,
                                Function<Error<T>, M> e,
                                Function<Quit<T>, M> q);


    public static final class Success<S> extends IOMessage<S> {
        public final S payload;

        @Override
        public Boolean isSuccess() {
            return true;
        }

        public <M> M match(Function<Success<S>, M> s,
                           Function<Error<S>, M> e,
                           Function<Quit<S>, M> q) {
            return s.apply(this);
        }

        public Success(S payload) {
            this.payload = payload;
        }

    }

    public static final class Error<S> extends IOMessage<S> {
        public final Throwable t;

        @Override
        public Boolean isError() {
            return true;
        }

        public <M> M match(Function<Success<S>, M> s,
                           Function<Error<S>, M> e,
                           Function<Quit<S>, M> q) {
            return e.apply(this);
        }

        public Error(Throwable t) {
            this.t = t;
        }
    }

    public static final class Quit<S> extends IOMessage<S> {

        @Override
        public Boolean isQuit() {
            return true;
        }

        public <M> M match(Function<Success<S>, M> s,
                           Function<Error<S>, M> e,
                           Function<Quit<S>, M> q) {
            return q.apply(this);
        }

        public Quit() {
        }
    }

}

