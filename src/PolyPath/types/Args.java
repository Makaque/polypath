package PolyPath.types;

import java.util.List;

public class Args {
    public final Point start;
    public final Point end;
    public final List<Polygon> shapes;

    public Args(Point start, Point end, List<Polygon> shapes) {
        this.start = start;
        this.end = end;
        this.shapes = shapes;
    }

    @Override
    public String toString() {
        return "Args(Start " + start + ",End " + end + ",Shapes " + shapes + ")";
    }
}
