package PolyPath.types;

/**
 * Created on 01/02/2018.
 */
public class Point {
    public final Double x;
    public final Double y;

    public Point(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public static final Point origin = new Point(0d, 0d);

    @Override
    public String toString() {
        return "Point(" + x + "," + y + ")";
    }
}
