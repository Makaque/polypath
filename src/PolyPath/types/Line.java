package PolyPath.types;

/**
 * Created on 06/02/2018.
 */
public class Line {
    public final Point p1;
    public final Point p2;

    public Line(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    @Override
    public String toString() {
        return "Line(" + this.p1 + "," + this.p2 + ")";
    }
}
