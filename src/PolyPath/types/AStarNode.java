package PolyPath.types;

/**
 * Created on 09/02/2018.
 */
public class AStarNode {
    public final Point n;
    public final AStarNode parent;
    public final Double stepLength;
    public final Double g;
    public final Double h;
    public final Double f;

    public AStarNode(Point n, AStarNode parent, Double stepLength, Double g, Double h) {
        this.n = n;
        this.parent = parent;
        this.stepLength = stepLength;
        this.g = g;
        this.h = h;
        this.f = g + h;
    }

}
