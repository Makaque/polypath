package PolyPath.types;

import java.util.List;

/**
 * Created on 01/02/2018.
 */
public class Polygon {
    public final List<Point> vertices;

    public Polygon(List<Point> vertices){
        this.vertices = vertices;
    }

    @Override
    public String toString() {
        return "Polygon(" + (vertices.stream().map(point -> "," + point).reduce("", (a, b) -> a + b) + ")").replaceFirst(",", "");
    }
}
