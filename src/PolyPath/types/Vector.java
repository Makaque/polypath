package PolyPath.types;

/**
 * Created on 02/02/2018.
 */
public class Vector extends Line {
    public Vector(Point from, Point to) {
        super(from, to);
    }

    @Override
    public String toString() {
        return "Vector(" + this.p1 + "," + this.p2 + ")";
    }
}
