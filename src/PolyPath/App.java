package PolyPath;

import PolyPath.types.AStarNode;
import PolyPath.types.Args;
import PolyPath.types.Point;
import PolyPath.types.Polygon;
import PolyPath.utils.IOMessage;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static PolyPath.Functions.*;

/**
 * Created on 01/02/2018.
 */
public class App {

    private Function<IO, IOMessage<Args>> readArgs = io -> {
        IOMessage<List<Polygon>> shapesMsg = io.readShapes();
        return shapesMsg.flatMap(shapes ->
                io.readEndPoints().map(ends ->
                        new Args(ends.payload.left, ends.payload.right, shapes.payload)));
    };

    public App() {
    }


    private Comparator<AStarNode> minFn = (n1, n2) -> {
        if (dEquals.apply(n1.f, n2.f)) {
            return 1;
        } else {
            return Double.compare(n1.f, n2.f);
        }
    };

    private AStarNode aStar(IO io, Point end, Set<AStarNode> steps, List<Point> visited, List<Polygon> shapes) {
        if(steps.isEmpty()){
            throw new RuntimeException("Failed to find goal");
        }
        Function<Point, Double> straightLineDistance = distanceBetweenPoints.apply(end);

        SortedSet<AStarNode> sortedSteps = new TreeSet<>(minFn);
        sortedSteps.addAll(steps);

        AStarNode minNode = sortedSteps.first();

        io.reportChosenNode(minNode);
        io.reportPathSoFar(reconstructPath.apply(minNode));

        if (pointEquals.apply(minNode.n, end)) {
            return minNode;
        } else {
            Set<AStarNode> successors = actions.apply(shapes).apply(end).apply(visited).apply(minNode.n).stream()
                    .map(v -> {
                        Double stepLength = vectorMagnitude.apply(v);
                        Double g = minNode.g + stepLength;
                        Double h = straightLineDistance.apply(v.p2);
                        return new AStarNode(v.p2, minNode, stepLength, g, h);
                    })
                    .filter(node -> visited.stream().noneMatch(vis -> pointEquals.apply(node.n, vis)))
                    .collect(Collectors.toSet());

            io.reportSuccessors(successors);
            io.separator();

            Set<AStarNode> updatedSteps = sortedSetAppendAll(sortedSetTail(sortedSteps), successors);

            List<Point> updatedVisited = listAppend(visited, minNode.n);

            return aStar(io, end, updatedSteps, updatedVisited, shapes);
        }


    }

    private AStarNode aStar(IO io, Args args){
        Set<AStarNode> steps = new HashSet<>();
        steps.add(new AStarNode(args.start, null, 0d, 0d, distanceBetweenPoints.apply(args.start).apply(args.end)));
        AStarNode node = aStar(io, args.end, steps, new ArrayList<>(), args.shapes);
        io.reportFoundPath(reconstructPath.apply(node));
        return node;
    }

    public void run(IO io) {
        // Read arguments
        // parse arguments
        // Report arguments
        // now I have start, end, and shapes
        // beginning with start, call actions for possible paths
        // Report possible paths
        // sort paths based on "straight line distance"
        // Report chosen path
        // repeat for chosen vertex

        IOMessage<Args> argsMsg = new IOMessage.Success<Args>(null);
        while (argsMsg.isSuccess()) {
            argsMsg = readArgs.apply(io);

            argsMsg.forEach(argsSuccess -> {
                Args args = argsSuccess.payload;
                io.reportArguments(args);
                aStar(io, args);

            });

        }

        if (argsMsg.isQuit()) {
            io.reportShutDown();
        } else if (argsMsg.isError()) {
            IOMessage.Error<Args> error = (IOMessage.Error<Args>) argsMsg;
            io.reportArgsError(error);
            io.reportShutDown();
        }


    }

}
