package PolyPath;

import PolyPath.types.*;
import PolyPath.utils.IOMessage;
import PolyPath.utils.Recursive;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TextStreamIO implements IO {

    private final InputStream input;
    private final PrintStream output;
    private final Scanner inputScanner;
    private final DecimalFormat round = new DecimalFormat(".##");


    public TextStreamIO(InputStream input, PrintStream output) {
        this.input = input;
        this.output = output;
        this.inputScanner = new Scanner(input);
    }

    private Function<PrintStream, Consumer<String>> prompter = (out) -> out::println;

    private Consumer<Consumer<String>> promptShapesDirections = (Consumer<String> prompt) -> {
        prompt.accept("Input shapes as semicolon delimited lists of comma separated points.");
        prompt.accept("Points should follow the vertices of a convex polygon in counter clockwise fashion.");
        prompt.accept("Enter 'd' or 'D' on its own line to finish.");
        prompt.accept("Enter 'q' or 'Q' on its own line to quit program.");
        prompt.accept("Ex:");
        prompt.accept("3 1, 4 3, 3 5, 2 4, 2 2;");
        prompt.accept("8 5, 10 7, 8 10, 6 8, 7 6;");
        prompt.accept("d");
    };

    private Consumer<Consumer<String>> promptEndpointsDirections = (Consumer<String> prompt) -> {
        prompt.accept("Input the start and end points as comma separated number pairs.");
        prompt.accept("Ex: 0 0, 63 72");
    };

    private Function<String, Boolean> isQuitMessage = (s) ->
            s.trim().toUpperCase().equals("Q");


    private Function<String, Boolean> shapesFinished = (s) ->
            s.trim().toUpperCase().equals("D");

    private Function<String, Boolean> endPointsFinished = (s) -> {
        String dbl = "-?\\d+(\\.\\d+)?";
        String point = dbl + "\\s+" + dbl;
        String pointPair = point + "\\s*,\\s*" + point;
        String regex = "^\\s*" + pointPair + "\\s*$";
        return s.matches(regex);
    };

    private Function<
            BiFunction<String, String, String>, Function<
            BiFunction<String, String, String>, Function<
            Function<String, Boolean>, Function<
            String, Function<
            Scanner, Function<
            String, IOMessage<String>>>>>>>
            nextInput =
            evaluator -> onSuccess -> inputFinished -> failureMessage -> scanner -> accumulator -> {
                Recursive<Function<String, IOMessage<String>>> next = new Recursive<>();
                next.func = (acc) -> {

                    try {
                        String line = scanner.nextLine();
                        if (isQuitMessage.apply(line)) {
                            return new IOMessage.Quit<>();
                        } else {
                            if (inputFinished.apply(evaluator.apply(acc, line))) {
                                return new IOMessage.Success<>(onSuccess.apply(acc, line));
                            } else {
                                return next.func.apply(acc + line);
                            }
                        }
                    } catch (Throwable t) {
                        Exception e = new Exception(failureMessage);
                        return new IOMessage.Error<>(e);
                    }
                };

                return next.func.apply(accumulator);

            };

    private Function<Scanner, Function<String, IOMessage<String>>> nextShapesInput = scanner -> accumulator ->
            nextInput.apply((acc, ln) -> ln)
                    .apply((acc, ln) -> acc)
                    .apply(shapesFinished)
                    .apply("Couldn't read shapes input.")
                    .apply(scanner)
                    .apply(accumulator);

    private Function<Scanner, Function<String, IOMessage<String>>> nextEndPointsInput = scanner -> accumulator ->
            nextInput.apply((acc, ln) -> acc + ln)
                    .apply((acc, ln) -> acc + ln)
                    .apply(endPointsFinished)
                    .apply("Couldn't read end points input.")
                    .apply(scanner)
                    .apply(accumulator);


    private Function<Scanner, IOMessage<String>> readShapesInput = (scanner) ->
            nextShapesInput.apply(scanner).apply("");

    private Function<Scanner, IOMessage<String>> readEndpointInput = (scanner) ->
            nextEndPointsInput.apply(scanner).apply("");


    private Function<String, Point> parseSinglePointInput = (point) -> {
        List<String> coordinateList = Arrays.asList(point.trim().split(" "));
        if (coordinateList.size() != 2) {
            throw new RuntimeException("Points given must contain exactly two coordinates");
        } else {
            return new Point(Double.valueOf(coordinateList.get(0)), Double.valueOf(coordinateList.get(1)));
        }
    };

    private Function<String, List<Point>> parsePointsInput = (points) ->
            Arrays.stream(points.split(","))
                    .map(parseSinglePointInput)
                    .collect(Collectors.toList());

    private Function<String, IOMessage<List<Polygon>>> parseSuccessfulShapesInput = (input) -> {
        try {
            List<Polygon> shapes =
                    Arrays.stream(input.split(";"))
                            .map(parsePointsInput)
                            .map(Polygon::new)
                            .collect(Collectors.toList());
            return new IOMessage.Success<>(shapes);

        } catch (Exception e) {
            Exception fail = new Exception("Failed to parse shapes input.", e);
            return new IOMessage.Error<>(fail);
        }
    };


    private Function<IOMessage<String>, IOMessage<List<Polygon>>> parseShapesInput = (input) ->
            input.flatMap(s ->
                    parseSuccessfulShapesInput.<IOMessage<List<Polygon>>>apply(s.payload));


    private Function<IOMessage<String>, IOMessage<Pair<Point>>> parseEndpointsInput = (input) ->
            input.flatMap(s -> {
                List<Point> points = parsePointsInput.apply(s.payload);
                if (points.size() != 2) {
                    return new IOMessage.Error<Pair<Point>>(new RuntimeException("Parsing endpoints resulted in more than 2 points"));
                } else {
                    return new IOMessage.Success<Pair<Point>>(new Pair<>(points.get(0), points.get(1)));
                }
            });

    private Function<Scanner, IOMessage<List<Polygon>>> readAndParseShapesInput = readShapesInput.andThen(parseShapesInput);
    private Function<Scanner, IOMessage<Pair<Point>>> readAndParseEndpointsInput = readEndpointInput.andThen(parseEndpointsInput);

    private void reporter(Consumer<String> prompt, String header, Runnable report) {
        prompt.accept(header);
        report.run();
        prompt.accept("");
    }

    private void reporter(Consumer<String> prompt, String header, String report) {
        reporter(prompt, header, () -> prompt.accept(report));
    }

    private Function<Consumer<String>, Consumer<Args>> argsReporter = prompt -> args -> {
        reporter(
                prompt,
                "Your arguments have been parsed:",
                args.toString()
        );
    };

    private Function<Consumer<String>, Consumer<AStarNode>> nodeReporter = prompt -> node ->
            prompt.accept(node.n + " | step: " + round.format(node.stepLength) + " | g: " + round.format(node.g) + " | h: " + round.format(node.h));

    private Function<Consumer<String>, Consumer<AStarNode>> chosenNodeReporter = prompt -> node ->
            reporter(prompt, "Chosen node:", () -> nodeReporter.apply(prompt).accept(node));

    private Function<Consumer<String>, Function<String, Consumer<Collection<AStarNode>>>> pathReporter = prompt -> header -> nodes ->
            reporter(prompt, header,
                    () -> nodes.forEach(nodeReporter.apply(prompt)));

    private Function<Consumer<String>, Consumer<Set<AStarNode>>> successorsReporter = prompt -> nodes ->
            pathReporter.apply(prompt).apply("Successors:").accept(nodes);

    private Function<Consumer<String>, Consumer<List<AStarNode>>> pathSoFarReporter = prompt -> nodes ->
            pathReporter.apply(prompt).apply("Path so far:").accept(nodes);

    private Function<Consumer<String>, Consumer<List<AStarNode>>> foundPathReporter = prompt -> nodes ->
            pathReporter.apply(prompt).apply("Found path:").accept(nodes);

    private Consumer<Consumer<String>> shutDownReporter = prompt ->
            prompt.accept("Shutting Down");


    private Function<Consumer<String>, Consumer<IOMessage.Error<Args>>> argsErrorReporter = prompt -> error -> {
        prompt.accept("Encountered an error:");
        prompt.accept(error.t.getMessage());
    };

    private Consumer<Consumer<String>> printSeparator = prompt -> {
            prompt.accept("**************************************************************************************");
            prompt.accept("");
    };



    private Consumer<String> getPrompt() {
        return prompter.apply(output);
    }

    private Scanner getScanner() {
        return inputScanner;
    }

    @Override
    public IOMessage<List<Polygon>> readShapes() {
        // prompt shapes directions
        // read until stop character
        // parse input into shapes
        promptShapesDirections.accept(getPrompt());
        return readAndParseShapesInput.apply(getScanner());
    }

    @Override
    public IOMessage<Pair<Point>> readEndPoints() {
        // prompt start and end input
        // read expected input
        // parse into points
        promptEndpointsDirections.accept(getPrompt());
        return readAndParseEndpointsInput.apply(getScanner());
    }

    @Override
    public void reportArguments(Args args) {
        argsReporter.apply(getPrompt()).accept(args);
    }

    @Override
    public void reportChosenNode(AStarNode node) {
        chosenNodeReporter.apply(getPrompt()).accept(node);
    }

    @Override
    public void reportSuccessors(Set<AStarNode> nodes) {
        successorsReporter.apply(getPrompt()).accept(nodes);
    }

    @Override
    public void reportPathSoFar(List<AStarNode> nodes) {
        pathSoFarReporter.apply(getPrompt()).accept(nodes);
    }

    @Override
    public void reportFoundPath(List<AStarNode> nodes) {
        foundPathReporter.apply(getPrompt()).accept(nodes);
    }

    @Override
    public void reportShutDown() {
        shutDownReporter.accept(getPrompt());
    }

    @Override
    public void reportArgsError(IOMessage.Error<Args> error) {
        argsErrorReporter.apply(getPrompt()).accept(error);
    }

    @Override
    public void separator() {
        printSeparator.accept(getPrompt());
    }
}
