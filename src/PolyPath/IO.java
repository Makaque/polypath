package PolyPath;

import PolyPath.types.*;
import PolyPath.utils.IOMessage;

import java.util.List;
import java.util.Set;

public interface IO {

    public IOMessage<List<Polygon>> readShapes();

    public IOMessage<Pair<Point>> readEndPoints();

    public void reportArguments(Args args);

    public void reportChosenNode(AStarNode node);

    public void reportSuccessors(Set<AStarNode> nodes);

    public void reportPathSoFar(List<AStarNode> nodes);

    public void reportFoundPath(List<AStarNode> nodes);

    public void reportShutDown();

    public void reportArgsError(IOMessage.Error<Args> error);

    public void separator();

}
