import PolyPath.App;
import PolyPath.TextStreamIO;

public class Main {

    public static void main(String[] args) {
        App cmdApp = new App();
        try{
            cmdApp.run(new TextStreamIO(System.in, System.out));
        } catch (Exception e){
            System.out.println("Unexpected exception.");
            System.out.println(e.getMessage());
            e.printStackTrace();
            System.out.println("Shutting down.");
        }
    }
}
